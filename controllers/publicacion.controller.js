'use strict'
let Global=require('../global');
let redis= require('redis');
let redisClient=redis.createClient(Global.Global.urlredis);
// let moment = require('moment');  
var moment = require('moment-timezone');

function consultajson(req,res){
    
    redisClient.get("descarga",function (err, reply) {
        //console.log(reply);
         //dato=reply;
        res.status(200).send({datos:reply});
    });    
}

function consultajsonGubernatura_det_entidad(req,res){
    
    redisClient.get("GOB_DET_ENTIDAD",function (err, reply) {
        //console.log(reply);
         //dato=reply;
        res.status(200).send({datos:JSON.parse(reply)});
    });    
}

function consultajsonGubernatura_distrito(req,res){
    
    redisClient.get("GOB_DISTRITO",function (err, reply) {
        //console.log(reply);
         //dato=reply;
        res.status(200).send({datos:JSON.parse(reply)});
    });    
}

function consultajsonGubernatura_entidad(req,res){
    
    redisClient.get("GOB_ENTIDAD",function (err, reply) {
        //console.log(reply);
         //dato=reply;
        res.status(200).send({datos:JSON.parse(reply)});
    });
}

function consultajsonGubernatura_seccion(req,res){
    
    redisClient.get("GOB_SECCION",function (err, reply) {
        //console.log(reply);
         //dato=reply;
        res.status(200).send({datos:JSON.parse(reply)});
    });    
}

/************************************************/


function consultajsonDiputados_det_entidad(req,res){
    
    redisClient.get("DIP_DET_ENTIDAD",function (err, reply) {
        //console.log(reply);
         //dato=reply;
        res.status(200).send({datos:JSON.parse(reply)});
    });    
}

function consultajsonDiputados_distrito(req,res){
    
    redisClient.get("DIP_DISTRITO",function (err, reply) {
        //console.log(reply);
         //dato=reply;
        res.status(200).send({datos:JSON.parse(reply)});
    });    
}

function consultajsonDiputados_entidad(req,res){
    
    redisClient.get("DIP_ENTIDAD",function (err, reply) {
        //console.log(reply);
         //dato=reply;
        res.status(200).send({datos:JSON.parse(reply)});
    });    
}

function consultajsonDiputados_seccion(req,res){
    
    redisClient.get("DIP_SECCION",function (err, reply) {
        //console.log(reply);
         //dato=reply;
        res.status(200).send({datos:JSON.parse(reply)});
    });    
}
/********************************************************/


function consultajsonMunicipio_detalle(req,res){
    
    redisClient.get("MUN_DETALLE",function (err, reply) {
        //console.log(reply);
         //dato=reply;
        res.status(200).send({datos:JSON.parse(reply)});
    });    
}
function consultajsonMunicipio_entidad(req,res){
    
    redisClient.get("MUN_ENTIDAD",function (err, reply) {
        //console.log(reply);
         //dato=reply;
        res.status(200).send({datos:JSON.parse(reply)});
    });    
}
function consultajsonMunicipio_seccion(req,res){
    
    redisClient.get("MUN_SECCION",function (err, reply) {
        //console.log(reply);
         //dato=reply;
        res.status(200).send({datos:JSON.parse(reply)});
    });    
}
function consultajsonMunicipio_municipios(req,res){
    
    redisClient.get("MUN_MUNICIPIOS",function (err, reply) {
        //console.log(reply);
         //dato=reply;
        res.status(200).send({datos:JSON.parse(reply)});
    });    
}

function consultarFechaHoraActual(req,res){
    res.status(200).send({        
        datos:{ 
            fecha : moment().subtract(1, 'hours').tz("America/Mexico_City").format("YYYYMMDDHHmm"), //Se resta una hora para el horario de verano
            fechaVerano: moment().tz("America/Mexico_City").format("YYYYMMDDHHmm")
        }
    });     
}

module.exports={
    consultajson,
    consultajsonGubernatura_det_entidad,
    consultajsonGubernatura_distrito,
    consultajsonGubernatura_entidad,
    consultajsonGubernatura_seccion,
    
    consultajsonDiputados_det_entidad,    
    consultajsonDiputados_distrito,
    consultajsonDiputados_entidad,
    consultajsonDiputados_seccion,
    
    consultajsonMunicipio_detalle,
    consultajsonMunicipio_entidad,
    consultajsonMunicipio_seccion,
    consultajsonMunicipio_municipios,
    consultarFechaHoraActual
}